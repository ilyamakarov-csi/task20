package tests;


import org.testng.Assert;
import org.testng.annotations.*;
import sources.Mathematics;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

/**
 * Created by IlyaMakarov on 12/13/2016.
 */
public class MathematicsTest {

    private Mathematics mathematics;

    @DataProvider(name = "createData")
    public static Object[][] data() throws IOException {
        Scanner sc = new Scanner(new File("src/test/java/resources/testdata.txt"));
        int countLines = 0;
        ArrayList<Object[][]> array = new ArrayList<Object[][]>();

        // get array size
        while (sc.hasNextLine()) {
            sc.nextLine();
            countLines++;
        }

        Object[][] data = new Object[countLines][2];
        int index = 0;
        sc = new Scanner(new File("src/test/java/resources/testdata.txt")); // reset scanner

        // get data
        while (sc.hasNextLine()) {
            data[index][0] = sc.nextInt(); //x
            data[index++][1] = sc.nextInt(); //y
        }

        return data;
    }

    @BeforeClass(alwaysRun = true)
    public void setDataForClass()    {
        mathematics = new Mathematics();
    }

    @BeforeMethod(alwaysRun = true)
    public void dropData()    {
        mathematics.setResult(0);
    }

    @Parameters({ "x", "y" })
    @Test(groups = {"smoke", "fast"})
    public void add(int x, int y) throws Exception {
        int expected = x + y;
        mathematics.add(x, y);
        int actual = mathematics.getResult();
        Assert.assertEquals(actual, expected, "result does not match expected");
    }

    @Test(groups = {"fast"}, dataProvider = "createData")
    public void deduct(int x, int y) throws Exception {
        int expected = x - y;
        mathematics.deduct(x, y);
        int actual = mathematics.getResult();
        Assert.assertEquals(actual, expected, "result does not match expected");
    }

    @Test(groups = {"fast"}, dataProvider = "createData")
    public void multiply(int x, int y) throws Exception {
        int expected = x * y;
        mathematics.multiply(x, y);
        int actual = mathematics.getResult();
        Assert.assertEquals(actual, expected, "result does not match expected");
    }

    @Test(expectedExceptions = ArithmeticException.class, groups = {"fast"}, dataProvider = "createData")
    public void divide(int x, int y) throws Exception {
        int expected = x / y;
        mathematics.divide(x, y);
        int actual = mathematics.getResult();
        Assert.assertEquals(actual, expected, "result does not match expected");
    }

    @Test(groups = {"fast"}, dependsOnMethods = {"setResult"})
    public void getResult() throws Exception {
        int expected = 0;
        Assert.assertEquals(mathematics.getResult(), expected, "result does not match expected");
    }

    @Test(groups = {"fast"})
    public void setResult() throws Exception {
        mathematics.setResult(5);
        int expected = 5;
        Assert.assertEquals(mathematics.getResult(), expected, "result does not match expected");
    }

    @Test(groups = {"fast"}, timeOut = 100)
    public void testForLongTimeExecution() throws Exception    {
        TimeUnit.SECONDS.sleep(100);
    }
}